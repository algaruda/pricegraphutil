﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BitcoinPriceGraph
{
   public class PriceGenerator
    {
        private int[] prices;
        int count;

        public int[] Prices
        {
            get
            {
                return prices;
            }
        }

        public PriceGenerator(int count, int heigth)
        {
            prices = new int[count];
            this.count = count;
            var margin = 30;
            var start = heigth / 2;

            var rand = new Random();
            prices[0] = start;
            var initial = rand.Next(0, 50);
            for (var i = 1; i < count; i++)
            {
                var delta = rand.Next(-margin, margin);

                if (prices[i - 1] + delta > heigth - margin)
                {
                    prices[i] = prices[i - 1] - delta;    
                }
                else if(prices[i - 1] + delta < margin)
                {
                    prices[i] = prices[i - 1] - delta;
                }
                else
                    prices[i] = prices[i - 1] + delta;
                
            }
        }

        
        public List<Price> Minimums()
        {
            var result = new List<Price>();
            for (var i = 0; i < count - 1; i++)
            {
                if(i == 0)
                {
                    if (prices[i] < prices[i + 1])
                    {
                        result.Add(new Price(i, prices[i]));
                    }
                }
                else if (prices[i - 1] > prices[i] && prices[i] < prices[i + 1])
                {
                    result.Add(new Price(i, prices[i]));
                }
            }
            return result;
        }

        public Price Max(Price min)
        {
            var current = new List<int>();
            for (var i = min.Index; i < count; i++)
            {
                current.Add(i);
            }

            var max = current.Max(i => prices[i]);
            var maxIndex = prices.ToList().LastIndexOf(max);

            var result = new Price(maxIndex, max);

            return result;
        }

        public Profit BestProfit()
        {
            var minimums = Minimums();
            var bestProfit = CountProfit(minimums.First());
            foreach (var min in minimums)
            {
                var currentProfit = CountProfit(min);
                if (currentProfit.Value() > bestProfit.Value())
                {
                    bestProfit = currentProfit;
                }
            }
            return bestProfit;
        }

        public Profit CountProfit(Price min)
        {
            return new Profit(min, Max(min));
        }
    }
}
