﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace BitcoinPriceGraph
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int SIZE = 100;
        private int HEIGHT = 500;
        private double STEP = 5;
        private int MARGIN = 30;
        

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonDraw_Click(object sender, RoutedEventArgs e)
        {
            cnvGraph.Children.Clear();
            HEIGHT = (int)(cnvGraph.RenderSize.Height);

            if(int.TryParse(txtCount.Text, out int result))
            {
                SIZE = result;
            }
            STEP = (cnvGraph.RenderSize.Width - MARGIN * 2) / SIZE;

            var generator = new PriceGenerator(SIZE, HEIGHT);
            var array = generator.Prices;
            var points = PrepareForOutput(array);
            DrawGraph(points);

            var bestProfit = generator.BestProfit();

            DrawPoint(MakePoint(bestProfit.Min.Index, bestProfit.Min.Value), Brushes.Yellow, 20);
            DrawPoint(MakePoint(bestProfit.Max.Index, bestProfit.Max.Value), Brushes.Red, 20);
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            cnvGraph.Children.Clear();
        }

        private void DrawGraph(List<Point> points)
        {
            foreach (var point in points)
                DrawPoint(point, Brushes.Blue, 4);

            PointCollection pointCollection = new PointCollection(points);
            var poly = new Polyline() { Points = pointCollection };
            poly.Stroke = Brushes.Black;
            poly.StrokeThickness = 1;
            cnvGraph.Children.Add(poly);
        }

        private void DrawTest(int x, int y)
        {
            DrawPoint(new Point(x, y), Brushes.Black, 5);
        }

        private void DrawPoint(Point point, Brush b, int size)
        {
            var ellipse = new Ellipse();
            ellipse.Height = size;
            ellipse.Width = size;
            var half = size / 2;
            ellipse.Stroke = b;
            ellipse.StrokeThickness = 1;
            ellipse.Margin = new Thickness(point.X-half, point.Y-half, 0, 0);

            cnvGraph.Children.Add(ellipse);
        }

        private List<Point> PrepareForOutput(int[] points)
        {
            var result = new List<Point>();

            for(int i = 0; i < points.Length; i++)
            {
                result.Add(MakePoint(i, points[i]));
            }

            return result;
        }

        private Point MakePoint(int x, int y)
        {
            return new Point(MARGIN + (x * STEP), HEIGHT - y);
        }
    }
}
