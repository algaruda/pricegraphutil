﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinPriceGraph
{
    public class Price
    {
        private int index;
        public int Index
        {
            get
            {
                return index;
            }
        }

        private int value;
        public int Value
        {
            get
            {
                return value;
            }
        }

        public Price(int index, int value)
        {
            this.index = index;
            this.value = value;
        }
    }
}
