﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BitcoinPriceGraph
{
    public class Profit
    {
        public Profit(Price min, Price max)
        {
            this.min = min;
            this.max = max;
        }

        private Price min;
        public Price Min
        {
            get
            {
                return min;
            }
        }

        private Price max;
        public Price Max
        {
            get
            {
                return max;
            }
        }

        public int Value()
        {
            return max.Value - min.Value;
        }
    }
}
